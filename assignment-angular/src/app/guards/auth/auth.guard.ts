import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';

/*
  Guard that is used to block access to pages when the user is not stored in local storage(logged in).
*/

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private _router:Router) {}

  /*
    Returns true of a user exists in local storage and allows access to other pages.
    Else return false and redirect user to login page.
  */
  canActivate(): boolean {
    if(!!localStorage.getItem('username')){
      return true;
    }else{
      this._router.navigate(['./login']);
      return false;
    }
  }
}
