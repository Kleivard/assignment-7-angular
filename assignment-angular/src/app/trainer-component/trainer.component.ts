import{ Component, OnInit } from '@angular/core';
import { PokemonDetail } from '../models/pokemon.detail';

    /*
        "Profile page containing details about cought pokemon"
    */

@Component({
    selector: 'app-trainer-component',
    templateUrl: './trainer.component.html',
    styleUrls: ['./trainer.component.scss']
})

export class TrainerCompoent implements OnInit {
    constructor() { }
    /*
        Parses all pokemons from local storage and displays them on the screen.
    */
    pokemons: PokemonDetail[] = [];

    ngOnInit(){ 
        let pokeArray: string[] = JSON.parse(localStorage.getItem('pokemonDetail') || '[]')
        for(let i = 0; i < pokeArray.length; i++){
            let tmpPokemon: PokemonDetail = JSON.parse(pokeArray[i]);
            console.log(Object.values(tmpPokemon)[2])
            console.log(tmpPokemon.sprites)
            this.pokemons.push(tmpPokemon);
        }
    }
}