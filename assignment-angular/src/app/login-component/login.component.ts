import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";

/*
    Component for login page. Handels login information and stores it in local storage.
*/

@Component({
    selector: 'app-login-component',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit{
    
    constructor(private _router: Router) {}

    /*
        On init check if user exist in local storage. If so redirect to catalogue page.
    */
    ngOnInit(): void {
        if(!!localStorage.getItem('username')){
            this._router.navigate(['./catalogue'])
        }
    }
    /*
        Submit function that stores the user in local storage and routes the user to the catalogue page.
    */
    public onSubmit(createForm: NgForm): void {
        localStorage.setItem("username", createForm.value.email)
        this._router.navigateByUrl('/catalogue')
    }
}