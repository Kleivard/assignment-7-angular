import { Component } from "@angular/core";

/*
    Empty container for catalogue components.
*/

@Component({
    selector: 'app-catalogue-page',
    templateUrl: './catalogue.page.html',
    styleUrls: ['./catalogue.page.scss']
})
export class CataloguePage{}