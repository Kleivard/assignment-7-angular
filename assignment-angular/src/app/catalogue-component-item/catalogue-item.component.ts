import { Component, Input, OnInit } from '@angular/core'
import { PokemonDetail } from '../models/pokemon.detail';
import { PokemonTestService } from '../services/pokemonTest.service';

/*
    catalogue-item contains a single instance of a Pokemon.
*/

@Component({
    selector:'app-catalogue-item',
    templateUrl: './catalogue-item.component.html',
    styleUrls: ['./catalogue-item.component.scss']
})

export class CatalogueItemComponent implements OnInit{
    constructor(
        private readonly _pokemonService: PokemonTestService,
    ){}
    @Input() pokemons?: PokemonDetail

    private pokemonToAdd?: PokemonDetail
    
    /*  
        Loads a pokemon from local storage
        Checks if Pokemon is captured and renders the page accordingly.
    */
    ngOnInit(): void {
        let pokeObj:string[] = []
        pokeObj  = JSON.parse(localStorage.getItem('pokemonDetail') || '[]')
        for (let index = 0; index < pokeObj.length; index++) {
            let tempPokemon = JSON.parse(pokeObj[index])
            if(tempPokemon['captured'] === true && tempPokemon['id'] == this.pokemons?.id){
                this.pokemons!.captured = true
            }
        }
    }

    /*
        Adds the pokemon to the player's collection in local storage
    */
    public choosePokemon(pokemonName?:string): void{
        this._pokemonService.getPokemonDetail1(pokemonName)
            .subscribe((pokemon: PokemonDetail) => {
                this.pokemonToAdd = pokemon
                let pokeObj:string[] = []
                pokeObj  = JSON.parse(localStorage.getItem('pokemonDetail') || '[]')
                // Checks if Pokemon exists in player's collection
                for (let index = 0; index < pokeObj.length; index++) {
                    let tempPokemon = JSON.parse(pokeObj[index])
                    if(tempPokemon['name'] === this.pokemonToAdd?.name){
                        alert("Pokemon already added")
                        return;
                    }
                }
                pokeObj.push(JSON.stringify({name:this.pokemonToAdd?.name, id: this.pokemonToAdd?.id, sprites:this.pokemonToAdd?.sprites.front_default, captured:true}))
                localStorage.setItem("pokemonDetail", JSON.stringify(pokeObj))
                this.pokemons!.captured = true
            })
    }
}