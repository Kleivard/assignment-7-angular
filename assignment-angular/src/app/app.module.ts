import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginPage } from './login-page/login.page';
import { LoginComponent } from './login-component/login.component';
import { CataloguePage } from './catalogue-page/catalogue.page';
import { CatalogueComponent } from './catalogue-component/catalogue.component';
import { CatalogueItemComponent } from './catalogue-component-item/catalogue-item.component';
import { AuthGuard } from './guards/auth/auth.guard';
import {TrainerPage} from './trainer-page/trainer.page';
import { TrainerCompoent } from './trainer-component/trainer.component';

/*
  Contains al declarations for components, pages, modules and providers.
*/

@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    LoginComponent,
    CataloguePage,
    CatalogueComponent,
    CatalogueItemComponent,
    TrainerPage,
    TrainerCompoent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
