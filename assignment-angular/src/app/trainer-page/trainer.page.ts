import { Component, OnInit } from '@angular/core';
/*
    Empty container for trainer components.
*/

@Component({
    selector: 'app-trainer-page',
    templateUrl: './trainer.page.html',
    styleUrls: ['./trainer.page.scss']
})

export class TrainerPage implements OnInit {
    constructor() { }
    ngOnInit() { }
}