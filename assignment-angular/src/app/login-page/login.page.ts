import { Component } from "@angular/core";

/*
    Empty container for login components.
*/

@Component({
    selector: 'app-login-page',
    templateUrl: './login.page.html'
})
export class LoginPage{}