import { Component, OnInit } from '@angular/core'
import { PokemonTestService } from '../services/pokemonTest.service';
import { PokemonDetail } from '../models/pokemon.detail';
import { PokemonList } from '../models/pokemon.list';
import { Observable, forkJoin } from 'rxjs';
import { Router } from '@angular/router';

@Component({
    selector:'app-catalogue-component',
    templateUrl: './catalogue.component.html',
    styleUrls: ['./catalogue.component.scss']
})

export class CatalogueComponent implements OnInit{

    pokemons: PokemonDetail[] = [];

    constructor(
        private readonly _pokemonService: PokemonTestService,
        private _router: Router,
        ){}
    
    ngOnInit(): void{
        this.loadPokemon();
    }

    /*
        Gets all pokemon
    */
    loadPokemon(){
        this._pokemonService.getPokemonList()
            .subscribe((list: PokemonList[]) => {
                this.getPokemon(list)
            })
    }

    /*
        Adds a filtered version of the pokemon to an array
    */
    private getPokemon(list: PokemonList[]) {
        const arr: Observable<PokemonDetail>[] = [];

        list.map((value: PokemonList) => {
            arr.push(this._pokemonService.getPokemonDetail(value.name));
        });
        
        forkJoin([...arr]).subscribe((pokemons) => {
            this.pokemons.push(...pokemons);
        })
    }

    /*
        Routes the user to the trainer page
    */
    public toTrainerPage(): void {
        this._router.navigateByUrl('/trainer')
    }
        
}