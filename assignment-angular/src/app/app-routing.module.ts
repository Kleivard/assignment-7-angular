import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginPage } from "./login-page/login.page";
import { CataloguePage } from "./catalogue-page/catalogue.page";
import { AuthGuard } from "./guards/auth/auth.guard";
import { TrainerPage } from "./trainer-page/trainer.page";


/*
    Routing table for all possible paths.
    Implements AuthGard for authentication checks.
*/

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/login'
    },
    {
        path: 'login',
        component: LoginPage
    },
    {
        path: 'catalogue',
        component: CataloguePage,
        canActivate: [AuthGuard]
    },
    {
        path: 'trainer',
        component: TrainerPage,
        canActivate: [AuthGuard]
    }
]
// Ending with module must go in the app.module.ts thingy
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}