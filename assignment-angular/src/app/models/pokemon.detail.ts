
/*
    Detailed Pokemon interface. Used to obtain specific parameters from Pokemon API call.
*/

export interface PokemonDetail  {
    id: number;
    name: string;
    captured: boolean;
    sprites: Sprite;
}

interface Sprite {
    front_default: string;
}