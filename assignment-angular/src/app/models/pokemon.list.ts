/*
   Mimics the API response used to obtain name and url from a collection of Pokemon. 
*/
export interface PokemonList  {
    name: string;
    url: string;
}