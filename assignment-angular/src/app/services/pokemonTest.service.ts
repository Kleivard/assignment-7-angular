import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { PokemonList } from "../models/pokemon.list";
import { PokemonDetail } from "../models/pokemon.detail";
import { map } from "rxjs/operators";

    /*
        Service file used to make API calls to collect information about pokemon.
    */

@Injectable(
    {
        providedIn: 'root'
    })

export class PokemonTestService {
    private firstGenUrl = 'https://pokeapi.co/api/v2/pokemon?limit=151';
    private baseUrl = 'https://pokeapi.co/api/v2/';
    
    constructor(private _http: HttpClient) { }

    /*
        Returns all pokemons from the first Gen. returns Name and Url to Pokemon details.
    */
    getPokemonList() : Observable<PokemonList[]> {
        return this._http.get<PokemonList[]>(this.firstGenUrl)
        .pipe(
            map((x: any) => x.results)
        );
    }
    /*
        Return specific Pokemon information based on id or name of Pokemon.
    */
    getPokemonDetail(pokemon: number | string): Observable<PokemonDetail> {
        return this._http.get<PokemonDetail>(this.baseUrl + 'pokemon/' + pokemon);
    }

    /*
        Same as getpokemonDetail. Written for learning purpose.
    */
    public getPokemonDetail1(pokemonName: string | undefined): Observable<PokemonDetail>{
        return this._http.get<PokemonDetail>(this.baseUrl + 'pokemon/' + pokemonName);
    }  
}